# nodejs_basics_training

## Content regarding day 1 assignment 

Question 1 :

Create a calculator to perform basic arithmetic operations (+, -, * , /). You are expected to take input from the console interactively.

Question 2 :

jobs.json: Provides a list of job descriptions.
technologies.json: Provides a list of technologies.
sample_expected_output.txt: Sample output of your code.
Your task is to tag each job from jobs.json against the technologies list provided in technologies.json.

Expected Output
It should be an array of jobs(with all original details) along with tags and the timestamp of processing that record.
Fields of each record: company, type, description, tags, processing_timestamp
Output filename syntax: ${timestamp}_response.json
Sample filename: 1581921103583_response.json

jobs.json and technologies.json files are provided in day 1 branch 







